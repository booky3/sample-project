import { useState } from 'react';
import './App.css';
import AddListTask from './Components/AddListTask';
import List from './Components/ListTask';

function App() {

const [inputText,setInputText]= useState('')
const [allAddListTask, setAllAddListTask] = useState([])

  return (
    <>
    <AddListTask 
      inputText={inputText} 
      allAddListTask={allAddListTask} 
      setAllAddListTask={setAllAddListTask} 
      setInputText = {setInputText}
    />
 
    <List 
      setAllAddListTask={setAllAddListTask} 
      allAddListTask={allAddListTask}
      inputText={inputText} 
      setInputText = {setInputText}
    />
    </>
    
    
  );
}

export default App;