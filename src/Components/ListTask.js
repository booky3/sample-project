import React from 'react'
import  ViewList from './ViewListTask'


export default function List({setInputText, setAllAddListTask, allAddListTask, inputText}) {

  return (
    <div className="list-container">
      
      {

        (allAddListTask.length===0) ?
        <>
        </>
        :
        allAddListTask.map(list => (
          <ViewList 
          setAllAddListTask={setAllAddListTask}
          allAddListTask = {allAddListTask}
          key={list.id} 
          list = {list}
          />
        ))
        
      }
       
     </div>
  )
}