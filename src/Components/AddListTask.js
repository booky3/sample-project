import React from 'react'

export default function AddListTask({setInputText, setAllAddListTask, allAddListTask, inputText}) {

  const checkSpaces = (inputText) => {
    return /^\s*$/.test(inputText);
  }

  const inputHandler = (e) => {
    setInputText(e.target.value)
  }

  const submitEventHandler = (e) => {
    e.preventDefault ()

    if(inputText === '' ){
      let color=document.querySelector("#input-form-text")
      color.style.borderColor = "grey"
    }
    else{

      let result = checkSpaces(inputText)

      if (result === true){
        let color=document.querySelector("#input-form-text")
      color.style.borderColor = "grey"
      }
      else {
        let color=document.querySelector("#input-form-text")
        color.style.borderColor = "grey"
        setAllAddListTask([...allAddListTask, 
          {
            text: inputText,
            id: new Date()
          }])
         
        setInputText('')
      }
    }
  }

  return (

    <form>
    <div className="addlist-container">
        <form>
          <input 
            name="text"
            type="text" 
            className="input-text" 
            placeholder="add to list"
            onChange={inputHandler} 
            value={inputText}
            id="input-form-text"
            required
            />
          <button onClick={submitEventHandler} type="submit">Add</button>
        </form>
    </div>
    </form>
  )
}