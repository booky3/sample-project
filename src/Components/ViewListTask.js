import React, { useState } from 'react'

export default function ViewListTask({list, setAllAddListTask, allAddListTask}) {


    const [updateInput, setUpdateInput]=useState('')

    const cancelEdit = () => {
        setIsEdit(false)
    }

    const editSave = (id) => {
        allAddListTask.map(result => {
            if(result.id === id){
                result.text= updateInput
            }
        })
        setIsEdit(false)
    
    }

    const editOnchange = (e) => {
        setUpdateInput(e.target.value)
    }

    const [isEdit, setIsEdit] = useState(false)


    const deleteHandler = () => {
        setAllAddListTask(allAddListTask.filter(result => result.id !== list.id))
    }

    const editHandler = () => {
        setIsEdit(true)

    }
 
  return (
    <>
    {
        (isEdit === false) ?
        <>
        <div className='edit-list'>
            <div id='list-text'>{list.text}</div>
            <div id='button-container'>
                <button onClick={editHandler}
                className='btn-edit'>Edit</button>
                <button onClick={deleteHandler} className="btn-delete">Delete</button>
            </div>
        </div>
         </>
         :
         <div className="edit-list">
         <div id="list-text">   
         
             <input  
                 type ="text" 
                 name ="edit-text"
                 defaultValue={list.text}
                 className ="input-text"
                 id = {list.id}
                 onChange={editOnchange}/>
         </div>
         <div id="button-container">
                 <button 
                 onClick={ e => editSave(list.id)}
                 type="submit" 
                 className="btn-save">
                 Save</button>
     
         <button onClick={cancelEdit} className="btn-cancel">Cancel</button>
         </div>
         </div>
    }
        
    </>

 
  )
}